/*
Following file define resource to create :
  - A role, with no permissions, which can be assumed by users within the same account,
  - A policy, allowing users / entities to assume the above role,
  - A group, with the above policy attached,
  - A user, belonging to the above group.
*/

resource "aws_iam_role" "ci_role" {
  name = "test_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": { "AWS": "123456789102" },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "ci_policy" {
  name = "ci-policy-1"

  policy = <<EOF
{ 
  "Version": "2012-10-17",
  "Statement": [
    { 
      "Action": "sts:AssumeRole",
      "Effect": "Allow",
      "Resource": "arn:aws:iam::123456789102:role/test_role"
    }
  ]
}
EOF
}

resource "aws_iam_group" "ci_builder_group" {
  name = "ci-builder"
}

resource "aws_iam_group_policy_attachment" "ci_policy_attachment" {
  group = aws_iam_group.ci_builder_group.name
  policy_arn = aws_iam_policy.ci_policy.arn
}

resource "aws_iam_user" "ci_user" {
  name = "ci-user"
}

resource "aws_iam_group_membership" "team" {
  name = "tf-testing-group-membership"

  users = [
    aws_iam_user.ci_user.name
  ]

  group = aws_iam_group.ci_builder_group.name
}
