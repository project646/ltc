## Dockerfile

Inspired by https://github.com/salessandri/docker-litecoind/blob/master/Dockerfile

Note : Public pgp key downloaded and commited in this repo to avoid connect to http://pgp.mit.edu/ which was sometimes not available during my tests.

## Security Scan

grype is used as CLI tool to execute security scan in the pipeline. I built my own [image](https://gitlab.com/project646/grype-dind/-/tree/main) with grype (based on docker-git image) to be able to execute it just after building the image. My idea was to scan before pushing to the registry to be sure to push a image who passed the security scan and also keep a clear stage separation in the pipeline.
Unfortunately, it results on 2 docker builds (one in `security_scan` job and the other in `build_and_push` job) which is not efficient.
To avoid breaking pipelines, `--fail-on` flag has not been put on purpose.

## Secret detection

Included a yaml file provided by Gitlab `Security/Secret-Detection.gitlab-ci.yml` adding a job in the pipeline to detect secrets commited in the repo. 

## Statefulset

Inspired by https://kubernetes.io/docs/tutorials/stateful-application/basic-stateful-set/ and https://medium.com/@Judezhu87/running-bitcoind-node-on-kubernetes-1d833212b1a for volumes.

## Terraform

Inspired by https://registry.terraform.io/providers/hashicorp/aws/latest/docs and https://docs.aws.amazon.com/IAM/latest/UserGuide/introduction.html for policy synthax and example.
tf config has been validated and plan created a plan but changes haven't been applied/tested.

## Text manipulation

I wasn't very inspired to be honest :-), just used awk to parse accesslog and find entry where HTTP code is not 200.

```
cat access.log | awk '{if ($9 != 200) print $0}'
```

I've written the same in python in `find_access_errors.py`
