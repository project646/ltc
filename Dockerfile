FROM buildpack-deps:stable-curl
ARG LTC_VERSION

COPY litecoin_pgp_public.asc litecoin_pgp_public.asc

#Instructions on how to verify GPG signature from https://download.litecoin.org/README-HOWTO-GPG-VERIFY.txt and https://bitcoin.stackexchange.com/questions/65156/how-do-i-verify-the-gpg-sig-for-litecoin-core
RUN wget https://download.litecoin.org/litecoin-$LTC_VERSION/linux/litecoin-$LTC_VERSION-x86_64-linux-gnu.tar.gz && \
    wget https://download.litecoin.org/litecoin-$LTC_VERSION/linux/litecoin-$LTC_VERSION-x86_64-linux-gnu.tar.gz.asc && \
    wget https://download.litecoin.org/litecoin-$LTC_VERSION/SHA256SUMS.asc && \
    if [ $(cat SHA256SUMS.asc | grep litecoin-$LTC_VERSION-x86_64-linux-gnu.tar.gz | cut -d' ' -f1) != $(sha256sum litecoin-$LTC_VERSION-x86_64-linux-gnu.tar.gz | cut -d' ' -f1) ]; then echo exit 1; fi && \
    gpg --import litecoin_pgp_public.asc && \
    gpg --verify litecoin-$LTC_VERSION-x86_64-linux-gnu.tar.gz.asc && \
    tar xvf litecoin-$LTC_VERSION-x86_64-linux-gnu.tar.gz && \
    mv litecoin-$LTC_VERSION/bin/* /usr/local/bin/ && \
    mkdir /var/litecoin && \
    rm -rf SHA256SUMS.asc litecoin-* /root/.gnupg/

EXPOSE 9333

ENTRYPOINT ["/usr/local/bin/litecoind", "-datadir=/var/litecoin"] 

